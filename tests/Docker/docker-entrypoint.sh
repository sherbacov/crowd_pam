#!/bin/bash
set -e
export CROWD_LICENSE=$(echo $CROWD_LICENSE | sed -e 's/\//\\\//g' | sed -e 's/\+/\\\+/g'| sed -e 's/\=/\\\=/g')
sed -i -e "s/<property name=\"license\">lkey<\/property>/<property name=\"license\">$CROWD_LICENSE<\/property>/" $CROWD_HOME/crowd.cfg.xml && \
chown -R atlassian $CROWD_HOME && \
umask 0027 && \
service slapd start && \
mkdir -p /var/log/atlassian/crowd && \
chown atlassian:root /var/log/atlassian/crowd -R && \
exec /opt/atlassian/apache-tomcat/bin/catalina.sh run >> /var/log/atlassian/crowd/catalina.sh
